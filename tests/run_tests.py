import unittest

from tests.test_agent_check import TestAgentCheck
from tests.handler.test_handler import TestAyeHandler
from tests.handler.test_help_handler import TestHelpHandler
from tests.test_db_mgr import TestDatabaseMgr
from tests.test_json_reader import TestMessageReader
from tests.test_parser import TestParser


def create_test_suite():
    test_suite = unittest.TestSuite()

    test_suite.addTest(TestAgentCheck)
    test_suite.addTest(TestAyeHandler)
    test_suite.addTest(TestHelpHandler)
    test_suite.addTest(TestDatabaseMgr)
    test_suite.addTest(TestMessageReader)
    test_suite.addTest(TestParser)

    return test_suite


if __name__ == '__main__':
    runner = unittest.TextTestRunner()
    test_suite = create_test_suite()

    runner.run(test_suite)
